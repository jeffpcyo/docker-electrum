FROM ubuntu
MAINTAINER Michal Belica <devel@beli.sk>
EXPOSE 10000

# runs as UID 1000 GID 1000 inside the container

ENV VERSION 3.3.4

RUN DEBIAN_FRONTEND=noninteractive && apt-get update && apt-get upgrade -y \
	&& apt-get install -y --no-install-recommends gpg gpg-agent dirmngr \
	#&& gpg --keyserver hkp://pool.sks-keyservers.net --recv-keys 0x2bd5824b7f9470e6 \
	#&& gpg --list-keys '0x6694D8DE7BE8EE5631BED9502BD5824B7F9470E6' \
	&& apt-get install -y --no-install-recommends curl dbus-x11 python-dbus python-lz4 python-numpy python-pil python-netifaces xpra python3-pyqt5 python3-wheel python3-pip python3-setuptools libsecp256k1-dev \
	&& curl -o /tmp/Electrum-${VERSION}.tar.gz https://download.electrum.org/${VERSION}/Electrum-${VERSION}.tar.gz \
	#&& curl -o /tmp/Electrum-${VERSION}.tar.gz.asc https://download.electrum.org/${VERSION}/Electrum-${VERSION}.tar.gz.asc \
	#&& gpg --verify /tmp/Electrum-${VERSION}.tar.gz.asc /tmp/Electrum-${VERSION}.tar.gz \
	&& pip3 install /tmp/Electrum-${VERSION}.tar.gz \
	&& test -f /usr/local/bin/electrum \
	#&& rm -vrf /tmp/Electrum-${VERSION}.tar.gz /tmp/Electrum-${VERSION}.tar.gz.asc ${HOME}/.gnupg \
	#&& apt-get purge --autoremove -y python3-pip python3-setuptools curl gpg gpg-agent dirmngr \
	&& apt-get clean && rm -rf /var/lib/apt/lists/* \
# disable starting of other programs in default xpra.conf# 
	&& sed -ie 's/^start-child/#start-child/' /etc/xpra/xpra.conf

RUN useradd -d /home/user -m user -G xpra \
	&& mkdir -p /run/user/`id -u user` && chown user:user /run/user/`id -u user` \
	&& mkdir /electrum \
	&& ln -s /electrum /home/user/.electrum \
	&& chown user:user /electrum \
	&& mkdir -p /run/xpra \
	&& chown user:user /run/xpra

USER user
ENV HOME /home/user
WORKDIR /home/user

VOLUME /electrum

CMD ["/usr/bin/xpra", "start", ":100", "--start-child=/usr/local/bin/electrum --oneserver", "--bind-tcp=0.0.0.0:10000", "--no-daemon", "--no-notifications", "--no-mdns", "--html=off", "--pulseaudio=no", "--speaker=disabled", "--microphone=disabled", "--webcam=no", "--exit-with-children"]
