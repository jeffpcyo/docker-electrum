Electrum docker image
=====================

Docker image of [Electrum][] bitcoin wallet.

[Electrum]: https://electrum.org


Locations
---------

Source of the image is hosted on Bitbucket at
https://bitbucket.org/beli-sk/docker-electrum

If you find any problems, please post an issue at
https://bitbucket.org/beli-sk/docker-electrum/issues

The built image is located on Docker Hub at
https://hub.docker.com/r/beli/electrum/


Pull or build
-------------

The image is built automatically on Docker hub in repository **beli/electrum**
and can be pulled using command

    docker pull beli/electrum

or if you'd prefer to build it yourself from the source repository

    git clone https://bitbucket.org/beli-sk/docker-electrum.git
    cd docker-electrum/
    docker build -t beli/electrum .


Usage
-----

The container uses volume `/electrum` as Electrum data directory.  The volumes
should be owned and writable by UID 1000, GID 1000.

The container exposes TCP port 10000 for [Xpra](https://xpra.org/).

Run the container in the background

    docker run -d -p 10000:10000 beli/electrum

and use Xpra on the host to attach to container's GUI

    xpra attach tcp:localhost:10000

